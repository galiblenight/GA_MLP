import numpy as np
import matplotlib.pyplot as plt
from random import randint

class MLP(object):

    def __init__(self, layers=[2, 2, 1], min_length=-1, max_length=1):
        self._layers = layers
        self._num_layer = len(self._layers) - 1
        self._min = min_length
        self._max = max_length
        self._range = max_length - min_length
        self._weights = [(np.random.random([layers[i] + 1, layers[i+1]]) * self._range + self._min) for i in range(self._num_layer)]

    def _sigmoid(self, v):
        return 1./(1. + np.exp(-v))

    def random_weights(self):
        self._weights = [(np.random.random([self._layers[i] + 1, self._layers[i+1]]) * self._range + self._min)
                         for i in range(self._num_layer)]

    def __str__(self):
        return str(self._weights)

    def feed_forward(self, X):
        for i in range(self._num_layer):
            X = np.concatenate([np.ones([X.shape[0], 1]), X], axis=1)
            X = self._sigmoid(X.dot(self._weights[i]))
        return X

    @staticmethod
    def from_list(layers, weight_list):
        mlp = MLP(layers)
        weight = []
        pointer = 0
        for i in range(len(layers)-1):
            next_pointer = pointer + (layers[i]+1) * layers[i+1]
            weight.append(weight_list[pointer:next_pointer].reshape([(layers[i]+1), layers[i+1]]))
            pointer = next_pointer
        mlp._weights = weight
        return mlp

class GP(object):

    def __init__(self, pop_size=100, layers=[2, 2, 1], min_length=-1, max_length=1):
        self._pop_size = pop_size
        self._layers = layers
        self._num_layer = len(self._layers) - 1
        self._min = min_length
        self._max = max_length
        self._range = max_length - min_length
        self._nextgen = []

        self._creature_size = np.sum([(layers[i]+1)*layers[i+1] for i in range(self._num_layer)])
        self._indiv = [np.random.random([self._creature_size]) * self._range + self._min
                        for i in range(self._pop_size)]

    def get_MLP_instance(self, num):
        return MLP.from_list(self._layers, self._indiv[num])

    def create_MLP_instance(self):
        try:
            del self._instances
        except: pass
        self._instances = [MLP.from_list(self._layers, self._indiv[i]) for i in range(len(self._indiv))]

    def fitness_from_CE(self, X, y):
        CEs = []
        for ins in self._instances:
            h = ins.feed_forward(X)
            ce = - np.mean(y*np.log(h) + (1 - y)*np.log(1 - h))
            CEs.append(ce)
        CEs = np.array(CEs)
        max_ce = np.max(CEs)
        return {"fitness": max_ce - CEs, "CE": CEs}

    def select(self, fitness):
        mean_fit = np.mean(fitness) * 10
        pointer = 0
        fit_pointer = np.random.random([1]) * mean_fit
        selected = []
        while pointer < fitness.shape[0]:
            if fit_pointer < fitness[pointer]:
                if len(selected) == 0 or pointer != selected[-1]:
                    selected.append(pointer)
                fit_pointer += mean_fit
            else:
                fit_pointer -= fitness[pointer]
                pointer += 1
        return np.array(selected)

    def elitist(self, fitness, num):
        self._nextgen = []
        fit = np.copy(fitness).tolist()
        for i in range(num):
            n = np.argmax(fit)
            fit.pop(n)
            self._nextgen.append(self._indiv[n])

    def onepoint_crossover(self, selected):
        sel = np.copy(selected).tolist()
        while len(sel) > 1:
            if len(sel) == 2:
                fst_par, snd_par = self._indiv[0], self._indiv[1]
                sel = []
            else:
                fst_par = self._indiv[sel.pop(randint(0, len(sel)-1))]
                snd_par = self._indiv[sel.pop(randint(0, len(sel)-1))]
            th = randint(0, self._creature_size)
            if np.random.random([1]) > 0.5:
                alpha1 = np.random.random([1])
                fst_chd, snd_chd = [np.zeros([self._creature_size]) for i in range(2)]
                fst_chd[:th] = fst_par[:th] * alpha1 + snd_par[:th] * (1 - alpha1)
                fst_chd[th:] = fst_par[th:] * (1 - alpha1) + snd_par[th:] * alpha1
                snd_chd[:th] = fst_par[:th] * (1 - alpha1) + snd_par[:th] * alpha1
                snd_chd[th:] = fst_par[th:] * alpha1 + snd_par[th:] * (1 - alpha1)
                self._nextgen.append(fst_chd)
                self._nextgen.append(snd_chd)

        self._indiv = self._nextgen

    def gen_indiv(self):
        space = self._pop_size - len(self._indiv)
        if space > 0:
            self._indiv.extend([np.random.random([self._creature_size]) * self._range + self._min
                            for i in range(space)])
